// Copyright (c) 2011 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

chrome.commands.onCommand.addListener(function (command) {
//    console.log(command);
    if (command == 'toggle-feature-foo') {
        chrome.tabs.query({active: true}, function (tab) {
            clearCacheMainFunction(tab);
        });
    }
});

// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function (tab) {
    clearCacheMainFunction(tab);

});

/**
 * Clear cache main function
 */
function clearCacheMainFunction(tab) {
    $.ajax({
        url: 'https://api.cloudflare.com/client/v4/zones//purge_cache',
        type: 'DELETE',
        data: '{"purge_everything":true}',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-Auth-Email', 'forpdfsending@gmail.com');
            xhr.setRequestHeader('X-Auth-Key', '');
            xhr.setRequestHeader('Content-Type', 'application/json');
        },
        success: function (result) {
            if (result.success) {

                chrome.notifications.create("",
                        {
                            type: 'basic',
                            title: "Cloudflare cache cleared!",
                            iconUrl: 'com.androidlord.cacheclear.png',
                            message: "Please, wait some time."
                        },
                        function () {}
                );

                setTimeout(function () {
                    var millisecondsPerWeek = 1000 * 60 * 60 * 24 * 888;
                    var period = (new Date()).getTime() - millisecondsPerWeek;
                    chrome.browsingData.remove({
                        "since": period
                    }, {
                        "appcache": true,
                        "cache": true,
//                    "cookies": true,
                        "downloads": true,
                        "fileSystems": true,
//                    "formData": true,
//                    "history": true,
                        "indexedDB": true,
                        "localStorage": true,
//                    "pluginData": true,
//                    "passwords": true,
                        "webSQL": true
                    }, function () {

                        chrome.tabs.reload(tab.id, {}, function () {
                            chrome.notifications.create("",
                                    {
                                        type: 'basic',
                                        title: "Cache cleared!",
                                        iconUrl: 'com.androidlord.cacheclear.png',
                                        message: "Cache cleared!"
                                    },
                                    function () {}
                            );
                        });


                    });
                }, 5000);


            } else {
                chrome.notifications.create("",
                        {
                            type: 'basic',
                            title: "Clear cache failed!",
                            iconUrl: 'com.androidlord.cacheclear.png',
                            message: "Can't clear cloudflare.com cache, please check api.cloudflare.com"
                        },
                        function () {}
                );
            }

        }
    });
}